Ansible demo project
====================




# Setting up

## 1 Setting up variables

First off, you need to configure the env_vars.
* Go to env_vars/base.yml, and replace the git_repo, project_name and application_name
* Generate a self-signed ssl certificate and fill out the private and public fields as indicated in base.yml

## 2 Make sure your project has the right structure

```
myproject
├── manage.py
├── myapp
│   ├── apps
│   │   └── __init__.py
│   ├── __init__.py
│   ├── settings
│   │   ├── base.py
│   │   ├── __init__.py
│   │   ├── development.py
│   │   └── production.py
│   ├── templates
│   │   ├── 403.html
│   │   ├── 404.html
│   │   ├── 500.html
│   │   └── base.html
│   ├── urls.py
│   └── wsgi.py
├── README.md
└── requirements.txt
```

The main things to note are the locations of the `manage.py` and `wsgi.py` files.  If your project's structure is a little different, you may need to change the values in these 2 files:

- `roles/web/tasks/setup_django_app.yml`
- `roles/web/templates/gunicorn_start.j2`

Also, if your app needs additional system packages installed, you can add them in `roles/web/tasks/install_additional_packages.yml`.
